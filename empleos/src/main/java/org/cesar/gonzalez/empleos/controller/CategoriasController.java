package org.cesar.gonzalez.empleos.controller;

import java.util.List;

import org.cesar.gonzalez.empleos.model.Categoria;
import org.cesar.gonzalez.empleos.service.IntCategoriasService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequestMapping(value = "/categorias")
public class CategoriasController {
	//anotacion anterior para solicitar peticiones
	@Autowired
	private IntCategoriasService categoriasService;
	
	@RequestMapping(value="/indexPaginate",method=RequestMethod.GET)
	public String mostrarIndexPaginado(Model model, Pageable page) {
		Page<Categoria>lista = categoriasService.buscarTodas(page);
		model.addAttribute("categorias", lista);
		return "categorias/listaCategorias";}
	
	
	@RequestMapping(value = "/eliminar", method = RequestMethod.GET)
	public String eliminar(@RequestParam("id")int idCategoria,
			RedirectAttributes attributes) {
		categoriasService.eliminar(idCategoria);
		attributes.addFlashAttribute("msg","Categoria eliminada");
		return "redirect:/categorias/indexPaginate";
		
	}
	
	
	@RequestMapping(value="/editar",method=RequestMethod.GET)
    public String editar(@RequestParam("id") int idCategoria,
            Model model) {
        Categoria  categoria =
                categoriasService.buscarPorId(idCategoria);
        model.addAttribute("categoria", categoria);
        return "categorias/formCategoria";
    }
	
	//@GetMapping("/index")
	@RequestMapping(value = "/index", method = RequestMethod.GET)
	public String mostrarIndex(Model model) { 
		List<Categoria> lista = categoriasService.obtenerTodas();
		for(Categoria c:lista) {
			System.out.println(c);
		}
		model.addAttribute("categorias", lista);
		return "categorias/listaCategorias";
	}
	//@GetMapping("/crear")
	@RequestMapping(value = "/crear", method = RequestMethod.GET)
	public String crear(Categoria categoria) {
		return "categorias/formCategoria";
	}
	/*
	//@PostMapping("/guardar")
	@RequestMapping(value = "/guardar", method = RequestMethod.POST)
	//hacer una viculacion de los elementos formularios conlas variables que reciben los parametros
	public String guardar(@RequestParam("nombre") String nombre,
			@RequestParam("descripcion") String descripcion) {
		System.out.println("Nombre: " + nombre);
		System.out.println("Descripcion: " + descripcion);
		return "categorias/listaCategorias";
	}
	*/
	@RequestMapping(value = "/guardar", method = RequestMethod.POST)
	public String guardar(Categoria categoria,RedirectAttributes attributes) {
		//categoria.setId(categoriasService.obtenerTodas().size()+1);
		categoriasService.guardar(categoria);
		attributes.addFlashAttribute("msg", "La categoria se guardo");
		return "redirect:/categorias/indexPaginate";
	}
	
}
