package org.cesar.gonzalez.empleos.service;

import java.util.List;

import org.cesar.gonzalez.empleos.model.Categoria;
import org.cesar.gonzalez.empleos.model.Vacante;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface IntVacantesService {
	//metodos abstractos
	public List<Vacante> obtenerTodas();
	public Vacante BuscarPorId(Integer id);
	public void guardar(Vacante vacante);
	public void eliminar(Integer id);
	public Page<Vacante>buscarTodas(Pageable page);	
	
}
