package org.cesar.gonzalez.empleos.service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.LinkedList;
import java.util.List;

import org.cesar.gonzalez.empleos.model.Categoria;
import org.cesar.gonzalez.empleos.model.Vacante;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class VacantesServiceImp implements IntVacantesService  {

		//varible de clase
	private List<Vacante> lista= null;
	public VacantesServiceImp(){
		 lista = new LinkedList<Vacante>();
		
		try {
			Vacante v1 = new Vacante();
			v1.setId(1);
			v1.setNombre("Contador");
			v1.setDescripcion("Relacionada con contabilidad general");
			v1.setFecha(LocalDate.parse("12-01-2021",DateTimeFormatter.ofPattern("dd-MM-yyyy")));
			v1.setSalario(10000.0);
			v1.setDestacado(1);
			v1.setEstatus("Aprobada");
			v1.setImagen("123163580_2819043258198031_127589973976501411_n.jpg");
			Categoria c1= new Categoria();
			c1.setId(1);
			c1.setNombre("Contabilidad");
			c1.setDescripcion("Relacionado con la contabilidad");
			v1.setCategoria(c1);
			lista.add(v1);
			
			Vacante v2 = new Vacante();
			v2.setId(2);
			v2.setNombre("Arquitecto");
			v2.setDescripcion("Amplia experiencia en construccion");
			v2.setFecha(LocalDate.parse("02-01-1997",DateTimeFormatter.ofPattern("dd-MM-yyyy")));
			v2.setSalario(12000.0);
			v2.setDestacado(0);
			v2.setEstatus("Creada");
			v2.setImagen("baki-dou-manga-300x457.jpg");
			Categoria c2= new Categoria();
			c2.setId(2);
			c2.setNombre("Tecnicos en computacion");
			c2.setDescripcion("Relacionado con el matenimiento de redes");
			
			v2.setCategoria(c2);
			lista.add(v2);
			
			Vacante v3 = new Vacante();
			v3.setId(3);
			v3.setNombre("Programador");
			v3.setDescripcion("Derarrollo de aplicaciones web");
			v3.setFecha(LocalDate.parse("25-01-2020",DateTimeFormatter.ofPattern("dd-MM-yyyy")));
			v3.setSalario(15000.0);
			v3.setDestacado(0);
			v3.setEstatus("Aprobada");
			v3.setImagen("IMG_0004.JPG");
			Categoria c3= new Categoria();
			c3.setId(3);
			c3.setNombre("Ingenieros");
			c3.setDescripcion("Ingenieros civiles");
			v3.setCategoria(c3);
			lista.add(v3);
			
			Vacante v4 = new Vacante();
			v4.setId(4);
			v4.setNombre("Tenico en mantenimiento");
			v4.setDescripcion("Amplio conocimiento tencnico");
			v4.setFecha(LocalDate.parse("01-01-2021",DateTimeFormatter.ofPattern("dd-MM-yyyy")));
			v4.setSalario(15000.0);
			v4.setDestacado(1);
			v4.setEstatus("Eliminada");
			v4.setImagen("Kang.png");
			Categoria c4= new Categoria();
			c4.setId(4);
			c4.setNombre("Arquitectos  ");
			c4.setDescripcion("Relacionado con el matenimiento de redes");
			v4.setCategoria(c4);
			lista.add(v4);
		}catch(DateTimeParseException ex) {
			System.out.println("Error: " + ex.getMessage());
		}
	}
	
	@Override
	public List<Vacante> obtenerTodas() {
		// TODO Auto-generated method stub
		return lista;
	}

	@Override
	public Vacante BuscarPorId(Integer id) {
		// TODO Auto-generated method stub
		for(Vacante vacante: obtenerTodas()) {
			if(vacante.getId()==id) {
				return vacante;
			}
		}
		return null;
	}

	@Override
	public void guardar(Vacante vacante) {
		// TODO Auto-generated method stub
		lista.add(vacante);
	}

	@Override
	public void eliminar(Integer id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Page<Vacante> buscarTodas(Pageable page) {
		// TODO Auto-generated method stub
		return null;
	}

}
