package org.cesar.gonzalez.empleos.controller;

import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.LinkedList;
import java.util.List;

import org.cesar.gonzalez.empleos.model.Vacante;
import org.cesar.gonzalez.empleos.service.IntVacantesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HomeController {
	
	@Autowired
	private IntVacantesService serviceVacantes;
	
	@GetMapping("/")
	public String mostrarHome(Model model) {
		//Devuelve una vista archivo.html
		List<Vacante> vacantes = serviceVacantes.obtenerTodas();
		model.addAttribute("vacantes", vacantes);
		return "Home";
	}
	@GetMapping("/mensaje")
	public String mensaje(Model model) {
		//formatear una fecha
		//LocalDate fecha=LocalDate.now();
		//personalizar fecha
		LocalDate fecha =null;
		try {
			fecha = LocalDate.parse("23-04-2021", DateTimeFormatter.ofPattern("dd-MM-yyyy"));
			System.out.print("Fecha: "+ fecha);
		}catch(DateTimeException ex) {
			System.out.print("Error : " + ex.getMessage());
		}
		
		model.addAttribute("fecha", java.sql.Date.valueOf(fecha));
		model.addAttribute("msg","Bienvenidos a mi aplicacion: Empleos App");
		return "mensaje";
	}
	//nuevo metodo
	
	@GetMapping("/vacante")
	public String vacante(Model model) {
		Integer id=1000;
		String nombre = "Programador de Aplicaciones web";
		LocalDate fecha= LocalDate.now();
		Double salario = 12500.00;
		Boolean vigente = true;
		model.addAttribute("id", id);
		model.addAttribute("nombre", nombre);
		model.addAttribute("fecha", fecha);
		model.addAttribute("salario", salario);
		model.addAttribute("vigente", vigente);
		return "vacante";
	}
	
	//Coleccion de datos
	@GetMapping("/lista")
	public String lista(Model model) {
		List<String> vacantes = new LinkedList<String>();
		vacantes.add("Contador publico");
		vacantes.add("Programador de Aplicaciones");
		vacantes.add("Tecnico De Mantenimiento de Computadores");
		vacantes.add("Arquitecto");
		vacantes.add("Ingeniero");
		for(String elemento:vacantes) {
			System.out.println(elemento);
		}
		model.addAttribute("vacantes",vacantes);
		return "vacante";
		
	}
	
	//un objeto de tipo vacante
	@GetMapping("/detalles")
	public String detalles(Model model) {
		Vacante v=new Vacante();
		System.out.println(v);
		v.setId(100);
		v.setNombre("Ingeniero electronico");
		v.setDescripcion("Relaciona con aplicaciones de IOT");
		v.setFecha(LocalDate.now());
		v.setSalario(12800.00);
		System.out.println(v);
		model.addAttribute("vacante", v);
		return "detalles";
	}
	//++++++++++++++++++++++++++++++++++++++++++++
	@GetMapping("/tabla")
	public String mostrarVacantes(Model model) {
		List<Vacante> vacantes = serviceVacantes.obtenerTodas();
		for(Vacante elemento : vacantes) {
			System.out.println(elemento.getId());
			System.out.println(elemento.getNombre());
			System.out.println(elemento.getDescripcion());
			System.out.println(elemento.getFecha());
			System.out.println(elemento.getSalario());
			System.out.println(elemento.getDestacado());
		}
		model.addAttribute("vacantes",vacantes);
		return "tabla";
	}
	
	
}
