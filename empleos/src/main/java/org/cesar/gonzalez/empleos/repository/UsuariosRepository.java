package org.cesar.gonzalez.empleos.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import org.cesar.gonzalez.empleos.model.*;
public interface UsuariosRepository extends JpaRepository<Usuario, Integer> {

}
