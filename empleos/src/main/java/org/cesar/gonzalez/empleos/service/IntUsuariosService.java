package org.cesar.gonzalez.empleos.service;

import java.util.List;

import org.cesar.gonzalez.empleos.model.Usuario;

public interface IntUsuariosService {
	public List<Usuario> obtenerTodas();
	public Usuario buscarPorId(Integer idUsuario);
	public void guardar(Usuario usuario);
	public void elimininar(Integer idUsuario);
}
