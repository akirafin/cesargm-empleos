package org.cesar.gonzalez.empleos.repository;

import org.springframework.data.jpa.repository.JpaRepository;


import org.cesar.gonzalez.empleos.*;
import org.cesar.gonzalez.empleos.model.*;

//public interface CategoriasRepository extends CrudRepository<Categoria, Integer> {
public interface CategoriasRepository extends JpaRepository<Categoria, Integer> {
	
}
