package org.cesar.gonzalez.empleos.service;

import java.util.LinkedList;
import java.util.List;

import org.cesar.gonzalez.empleos.model.Categoria;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class CategoriasServiceImp implements IntCategoriasService{
	
	List<Categoria> lista = null;
	
	public CategoriasServiceImp() {
		
	
 		lista =new LinkedList<Categoria>();
		
		Categoria c1= new Categoria();
		c1.setId(1);
		c1.setNombre("Contabilidad");
		c1.setDescripcion("Relacionado con la contabilidad");
		lista.add(c1);
		
		Categoria c2= new Categoria();
		c2.setId(2);
		c2.setNombre("Tecnicos en computacion");
		c2.setDescripcion("Relacionado con el matenimiento de redes");
		lista.add(c2);
		
		Categoria c3= new Categoria();
		c3.setId(3);
		c3.setNombre("Ingenieros");
		c3.setDescripcion("Ingenieros civiles");
		lista.add(c3);
		
		Categoria c4= new Categoria();
		c4.setId(4);
		c4.setNombre("Arquitectos  ");
		c4.setDescripcion("Relacionado con el matenimiento de redes");
		lista.add(c4);
		
	}

	@Override
	public List<Categoria> obtenerTodas() {
		// TODO Auto-generated method stub
		return lista;
	}

	@Override
	public Categoria buscarPorId(Integer idCategoria) {
		// TODO Auto-generated method stub
		for(Categoria categoria: lista) {
			if(categoria.getId()==idCategoria) {
				return categoria;
			}
		}
		return null;
	}

	@Override
	public void guardar(Categoria categoria) {
		// TODO Auto-generated method stub
		lista.add(categoria);
	}

	@Override
	public void eliminar(Integer idCategoria) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Page<Categoria> buscarTodas(Pageable page) {
		// TODO Auto-generated method stub
		return null;
	}
}
	/*
	@Override
	public List<Categoria> obtenerTodas() {
		// TODO Auto-generated method stub
		return lista;
	}

	@Override
	public Categoria buscarPorId(Integer idCategoria); {
		// TODO Auto-generated method stub
		for(Categoria categoria: lista) {
			if(categoria.getId()==idCategoria) {
				return categoria;
			}
		}
		return null;
	}

	@Override
	public void guardar(Categoria categoria) {
		// TODO Auto-generated method stub
		lista.add(categoria);
}
*/
